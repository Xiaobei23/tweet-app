<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@ page import="com.google.appengine.api.datastore.KeyFactory" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<title>Sandy's Tweet App</title>
<style type="text/css">
body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

.topnav {
  overflow: hidden;
  background-color: #4267b2;
}

.topnav a {
  float: left;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

.topnav a:hover {
  background-color: #29487d;
  color: white;
}

.topnav a.active {
  background-color: #ff9999;
  color: white;
}

.textstyle {
  margin-top: 20px;
  margin-left: 100px;
}

.timeline {
  margin-top: 20px;
  margin-left: 200px;
  color: #ff9999;
  font-size: 20px;
}
.sendbtn {
  background-color: #4267b2;
  border-color: #4267b2;
  padding:2px 5px;
  
}
.checkbxtext {
  color:#616770;
  padding:2px 5px;
  font-size: 14px;
}
.selectbtn {
  color:#616770;
  padding:2px 5px;
  font-size: 14px;
}
.btn-loc {
  display: block;
  margin-left: 100px;
}
.selectbtn:hover {
  background-color: #e0e0eb;
}
.logoutstyle {
  color:#616770;
  font-size: 14px;
}
.logoutstyle:hover {
  background-color: #e0e0eb;
}
/* Modal */
/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
    background-color: #fefefe;
    margin: auto;
    padding: 20px;
    border: 1px solid #888;
    width: 80%;
}

/* The Close Button */
.close {
    color: #aaaaaa;
    float: right;
    font-size: 28px;
    font-weight: bold;
}

.close:hover,
.close:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
}
</style>
</head>
<body>

<script>
function statusChangeCallback(response) {
	console.log('statusChangeCallback');
	console.log(response);
	// The response object is returned with a status field that lets the
	// app know the current login status of the person.
	// Full docs on the response object can be found in the documentation
	// for FB.getLoginStatus().
	if (response.status === 'connected') {
	// Logged into your app and Facebook.
		testAPI();
	} else if (response.status === 'not_authorized') {
	// The person is logged into Facebook, but not your app.
		document.getElementById('status').innerHTML = 'Please log ' +
		'into this app.';
	} else {
	// The person is not logged into Facebook, so we're not sure if
	// they are logged into this app or not.
		document.getElementById('status').innerHTML = 'Please log ' +
		'into Facebook.';
	}
}

// This function is called when someone finishes with the Login
// Button. See the onlogin handler attached to it in the sample
// code below.
function checkLoginState() {
	FB.getLoginStatus(function(response) {
		statusChangeCallback(response);
	});
}

window.fbAsyncInit = function() {
FB.init({
	appId : '427889427672700',
	cookie : true, // enable cookies to allow the server to access 
	// the session
	xfbml : true, // parse social plugins on this page
	version : 'v2.1' // use version 2.1
});

// Now that we've initialized the JavaScript SDK, we call 
// FB.getLoginStatus(). This function gets the state of the
// person visiting this page and can return one of three states to
// the callback you provide. They can be:
//
// 1. Logged into your app ('connected')
// 2. Logged into Facebook, but not your app ('not_authorized')
// 3. Not logged into Facebook and can't tell if they are logged into
// your app or not.
//
// These three cases are handled in the callback function.

FB.getLoginStatus(function(response) {
	statusChangeCallback(response);
	});
};

// Load the SDK asynchronously
(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = "https://connect.facebook.net/en_US/sdk.js";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

function testAPI() {
	console.log('Welcome! Fetching your information.... ');	
	FB.api('/me', function(response) {
	      console.log('Successful login for: ' + response.name);
	      var inputsUserName = document.querySelectorAll('input[name="userName"]');
	      inputsUserName.forEach(function(input) {
	    	 input.value = response.name; 
	      });
	      
	      /* const userId = document.querySelector('input[name="userId"]');
	      userId.value = response.id; */
	      
	      document.getElementById('status').innerHTML = '<img src="http://graph.facebook.com/' + response.id + '/picture?type=square">' + response.name + 
	    		  '&nbsp&nbsp<span onclick="fbLogout()"><a class="fb_button fb_button_medium logoutstyle"><span class="fb_button_text">Logout</span></a></span>';
	      
	      var xhr = new XMLHttpRequest();
	      var data = "userName=" + response.name;
	  	  xhr.open("POST", '/list', true);
	  	  xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	  	  xhr.onreadystatechange = function() {
	  		if (this.readyState == 4 && this.status == 200) {
	  			document.querySelector('select#tweetlist').innerHTML = this.responseText;
	  		}
	  	  };
	  	  xhr.send(data);
	    });
}

function fbLogout() {
    FB.logout(function (response) {
        //Do what ever you want here when logged out like reloading the page
        window.location.reload();
    });
}

function testMessageCreate() {
	 console.log('Posting a message to user feed.... '); 
	 //first must ask for permission to post and then will have call back function defined right inline code
	 // to post the message.
	 var typed_text = document.getElementById("message_text").value;
	 var frm = document.querySelector('form[name="form1"]');
	 if(document.getElementById('checkBox').checked){
	     FB.ui({method: 'share',   href: 'https://apps.facebook.com/tweet-app', quote: typed_text}, function(response){
	    	 frm.submit();
	     });
	 } else {
		 frm.submit();
	 }
}

function onFriendsClick() {
	FB.getLoginStatus(function(response) {
		if (response.status == 'connected') {
			var xhr = new XMLHttpRequest();
			var name = document.querySelector('input[name="userName"]').value;
			/* var userId = document.querySelector('input[name="userId"]').value; */
			/* var data = "userName=" + name + "&" + "userId=" + userId; */
			var data = "userName=" + name;
			xhr.open("POST", '/friends', true);
			xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			xhr.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
					document.querySelector('#friendPosts').innerHTML = this.responseText;
				}
			};
			xhr.send(data);
		}
		});
	
}

function onTopClick() {
	FB.getLoginStatus(function(response) {
		if (response.status == 'connected') {
			var xhr = new XMLHttpRequest();
			/* var data = "userName=" + name; */
			xhr.open("POST", '/top', true);
			xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			xhr.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
					document.querySelector('#topPosts').innerHTML = this.responseText;
				}
			};
			xhr.send();
		}
	});
}
function onFriendPostClick(evt) {
	evt.preventDefault();
	var key = evt.target.dataset.key;
	
	/* $.post("/info",
	        {
	        tweet: key
	    }, function(response, status) {
	        // notify user that his order is successfull
	        }
	); */

	/* var xhr = new XMLHttpRequest();
	var data = "tweet=" + key;
	xhr.open("POST", '/fpost', true);
	xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xhr.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			document.getElementById('fpost').innerHTML = this.responseText;
			showModal();
		}
	};
	xhr.send(data); */
}
function onInfoClick() {
	var inputTweet = document.querySelector('select[name="tweetlist"]');
	if (!inputTweet || !inputTweet.value)
		return;
	
	var frm = document.querySelector('form[name="myForm"]');
	frm.action = '/info';
	frm.submit();
	
/* 
	var xhr = new XMLHttpRequest();
	var data = "tweet=" + key;
	xhr.open("POST", '/info', true);
	xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xhr.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			document.querySelector('#theText').innerHTML = this.responseText;
			showModal();
		}
	};
	xhr.send(data); */
	
}
function onShareFriendClick(){
	var inputKey = document.querySelector('select#tweetlist');
	if (!inputKey || !inputKey.value) {
		alert('Something went wrong. Your message does not have the key');
		return;
	}
	FB.ui({
		method:'send',
		link:'https://tweet-204406.appspot.com/info?tweetlist='+inputKey.value,
	});
}

var option = null;
function myNewFunction(sel)
{
    option = sel.options[sel.selectedIndex].text;
}

function onShareFBClick(){
	var typed_text = option;
	/* var typed_text = document.querySelector('select[name="tweetlist"]').text; */
	FB.ui({
		method: 'share',   
		href: 'https://apps.facebook.com/tweet-app', quote: typed_text}, 
		function(response){});
	
}

function showModal() {
	var modal = document.getElementById('modalDlg');
	modal.style.display = 'block';
	modal.style.zIndex = '9999999';
}

function onModalCloseClick() {
	var modal = document.getElementById('modalDlg');
	modal.style.display = 'none';
}



</script>




<fb:login-button scope="public_profile,email" onlogin="checkLoginState();">
</fb:login-button>



<div id="status">
</div>
<!-- <span onclick="fbLogout()"><a class="fb_button fb_button_medium logoutstyle"><span class="fb_button_text">Logout</span></a></span> -->
<!-- <div class="topnav">
  <a class="active" href="TweetServlet">Tweet</a>
  <a href="FriendServlet">Friends</a>
  <a href="TopServlet">Top Tweets</a>
</div> -->

<ul class="topnav nav nav-tabs">
  <li class="active"><a class="nav-link" data-toggle="tab" href="#myTweet">Tweet</a></li>
  <li><a class="nav-link" data-toggle="tab" href="#friendTweet" onclick="onFriendsClick()">Friends</a></li>
  <li><a class="nav-link" data-toggle="tab" href="#topTweet" onclick="onTopClick()">Top Tweets</a></li>
</ul>
<div class="tab-content">
	<div id="myTweet" class="tab-pane active">
	 
		<form name="form1" action="/ds" method="post">
			<div class="textstyle">
				<textarea rows="4" cols="29" type="text" placeholder ="What's on your mind?" id="message_text" value="${send_message}" name="message_text""></textarea>
				
				 <div>
					<input id="checkBox" name="checkBox" type="checkbox"/><span class="checkbxtext">Post on your Facebook TimeLine</span><br>
					<input class="btn btn-primary sendbtn" type="button" name="opt" value="send" onclick="testMessageCreate();"/>
					<input type="hidden" name="opt" value="send" />
				</div>
			</div>
			<input type="hidden" name="userName" value="" />
			<!-- <input type="hidden" name="userId" value="" /> -->
		</form>
		
		<div class="timeline">Tweets</div>
		
		<form action="/ds" method="post" name="myForm">
			<select name="tweetlist" id="tweetlist" class="textstyle" size="8" style="width:315px;" onChange="myNewFunction(this);">
				<%-- <c:forEach items="${entityList}" var="entity">
				    <option value="${KeyFactory.keyToString(entity.getKey())}">${entity.getProperty("message")}</option>
				</c:forEach> --%>
			</select>
			<div class="btn-group btn-loc">
				<button type="button" class="btn btn-default selectbtn" onclick="onInfoClick()">Info</button>
				<button type="button" class="btn btn-default selectbtn" onclick="onShareFBClick()">Post to facebook</button>
				<button type="button" class="btn btn-default selectbtn" onclick="onShareFriendClick()">Send to friend</button>
				<input name="opt" value="Delete" type="submit" class="btn btn-default selectbtn"/>
			</div>
			<input type="hidden" name="userName" value="" />
		</form>
		
		<div id="theText"></div>
	</div>
	
	<div id="friendTweet" class="tab-pane">
		<!-- <div id="fpost"></div> -->
		<div id="friendPosts"></div>
	</div>
	<div id="topTweet" class="tab-pane">
		<div id="test"></div>
		<div id="topPosts"></div>
	</div>
</div>
</body>
</html>
