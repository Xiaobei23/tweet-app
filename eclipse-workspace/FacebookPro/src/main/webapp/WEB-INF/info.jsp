<%-- <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<div>
	<div id="modalDlg" class="modal">
	    <div class="modal-content">
	        <span class="close" onclick="onModalCloseClick()">&times;</span>
	        <span class="checkbxtext">Message: ${message}.</span>
	        <span class="checkbxtext">Created by: ${author}.</span>
	        <span class="checkbxtext">Created at: ${date}.</span>
	        <span class="checkbxtext">Visited times: ${view}.</span>
	    </div>
  </div>
</div> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@ page import="com.google.appengine.api.datastore.KeyFactory" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<title></title>
<style type="text/css">
body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}
.checkbxtext {
  color:#616770;
  padding:2px 5px;
  font-size: 14px;
}
ul li {
  display: block;
}
</style>
</head>
<body>
 <div class="container">
     <!-- <span class="close" onclick="onModalCloseClick()">&times;</span> -->
     <ul>
     <li class="checkbxtext">Message: ${message}.</li>
     <li class="checkbxtext">Created by: ${author}.</li>
     <li class="checkbxtext">Created at: ${date}.</li>
     <li class="checkbxtext">Visited times: ${view}.</li>
     </ul>
 </div>

</body>
</html>