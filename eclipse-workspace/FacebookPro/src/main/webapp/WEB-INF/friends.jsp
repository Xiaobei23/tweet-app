<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@ page import="com.google.appengine.api.datastore.KeyFactory" %>

<c:forEach items="${friendsList}" var="friend"> 
	<div class="container">
		<div>
		<%-- <img src="http://graph.facebook.com/${userId}/picture?type=square"/> --%>
		${friend.key}
		</div>
		<ul class="list-group">
			<c:forEach items="${friend.value}" var="entity">
				<li class="list-group-item" ><a href="/info?tweetlist=${KeyFactory.keyToString(entity.getKey())}" >${entity.getProperty("message")}</a></li>	
			</c:forEach>
		</ul>
	</div>
</c:forEach>