<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@ page import="com.google.appengine.api.datastore.KeyFactory" %>

<c:forEach items="${topList}" var="tl"> 
	<div class="container">
		${tl.getProperty("author")} <span class="checkbxtext"><i>${tl.getProperty("Date")}</i></span>
		<ul class="list-group">
			<li class="list-group-item"><a href="/info?tweetlist=${KeyFactory.keyToString(tl.getKey())}" >${tl.getProperty("message")}</a> <span class="checkbxtext">viewed ${tl.getProperty("view")} times</span></li>
		</ul>
	</div>
</c:forEach>