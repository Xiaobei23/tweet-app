<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@ page import="com.google.appengine.api.datastore.KeyFactory" %>
<c:forEach items="${entityList}" var="entity">
	<option value="${KeyFactory.keyToString(entity.getKey())}">${entity.getProperty("message")}</option>
</c:forEach>