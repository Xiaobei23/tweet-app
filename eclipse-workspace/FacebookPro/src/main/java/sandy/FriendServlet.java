package sandy;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Query;

/**
 * Servlet implementation class FriendServlet
 */
@WebServlet("/FriendServlet")
public class FriendServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	String user;
	Map<String, ArrayList<Entity>> map;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FriendServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
		user = request.getParameter("userName");
		
		map = new HashMap<>();
		Query q = new Query("Tweet");
		for (Entity entity : ds.prepare(q).asIterable()) {
            // do something with this entity
			if (!map.containsKey(entity.getProperty("author"))) {
				ArrayList<Entity> list = new ArrayList<>();
				list.add(entity);
				map.put(entity.getProperty("author").toString(), list);
			} else {
				map.get(entity.getProperty("author")).add(entity);
			}
        }
		map.remove(user);
		request.setAttribute("friendsList", map);
		RequestDispatcher jsp = request.getRequestDispatcher("/WEB-INF/friends.jsp");
		jsp.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
