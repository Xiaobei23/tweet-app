package sandy;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Query;

/**
 * Servlet implementation class TopViewServlet
 */
@WebServlet("/TopViewServlet")
public class TopViewServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	PriorityQueue<Entity> pq;
	ArrayList<Entity> list;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TopViewServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
		
		pq = new PriorityQueue<>(5, new Comparator<Entity>() {  
            public int compare(Entity e1, Entity e2) {                         
                return Long.valueOf((long)e1.getProperty("view")).intValue() -  Long.valueOf((long)e2.getProperty("view")).intValue();
            }
        });
		Query q = new Query("Tweet");
		for (Entity entity : ds.prepare(q).asIterable()) {
            // do something with this entity
			if (pq.size() < 5) {
				pq.offer(entity);
			} else {
				int pqValue = Long.valueOf((long)pq.peek().getProperty("view")).intValue();
				int entityValue = Long.valueOf((long)entity.getProperty("view")).intValue();
				if (pqValue < entityValue) {
					pq.poll();
					pq.offer(entity);
				}
			}
			
        }
		list = new ArrayList<>();
		while(!pq.isEmpty()) {
			list.add(pq.poll());
		}
		Collections.reverse(list);
		request.setAttribute("topList", list);
		RequestDispatcher jsp = request.getRequestDispatcher("/WEB-INF/top.jsp");
		jsp.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
