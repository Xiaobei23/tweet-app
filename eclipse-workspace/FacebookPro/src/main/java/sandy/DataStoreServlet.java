package sandy;

import java.awt.List;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.SimpleTimeZone;
import java.util.logging.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.memcache.MemcacheService;
import com.google.appengine.api.memcache.MemcacheServiceFactory;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;


/**
 * Servlet implementation class DataStoreServlet
 */
@WebServlet("/DataStoreServlet")
public class DataStoreServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;   
	String action;
	SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSSSSS");
	ArrayList<Entity> list;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DataStoreServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
		action = request.getParameter("opt");
		if (action.equals("send")) {
			PrintWriter out = response.getWriter();
	        //if (action != null) {
	        	
	        //STEP 2: Create Entity
	        Entity tweet = new Entity("Tweet");


	        //STEP 3: setup properties of Entity
	       // tweet.setProperty("id", );
	        tweet.setProperty("author", request.getParameter("userName"));
	        tweet.setProperty("message", request.getParameter("message_text"));
	        Date createDate = new Date();
	        tweet.setProperty("Date", createDate);
	        tweet.setProperty("view", 0);


	        //STEP 4: put Entity in DataStore
	        ds.put(tweet);
	        out.println("<p>Added a Book entity to the datastore via the low-level API, key: " + KeyFactory.keyToString(tweet.getKey()) + "</p>");
	        fmt.setTimeZone(new SimpleTimeZone(0, ""));
	        out.println("<p>The time is: " + fmt.format(new Date()) + "</p>");
		} else if (action.equals("Delete")) {
			String keyStr = request.getParameter("tweetlist");
			Key userKey = KeyFactory.stringToKey(keyStr);
		    ds.delete(userKey);
		} 
		
        
        list = new ArrayList<>();
        Query q = new Query("Tweet");
        for (Entity entity : ds.prepare(q).asIterable()) {
            // do something with this entity
        	list.add(entity);
        }
        request.setAttribute("entityList", list);
        
        RequestDispatcher jsp = request.getRequestDispatcher("/WEB-INF/index.jsp");
		jsp.forward(request, response);
	}

}
