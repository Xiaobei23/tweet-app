package sandy;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

/**
 * Servlet implementation class InfoServlet
 */
@WebServlet("/InfoServlet")
public class InfoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	String keyStr;
	String userName;
	Key key;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InfoServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		keyStr = request.getParameter("tweetlist");
		userName = request.getParameter("userName");
		DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
		key = KeyFactory.stringToKey(keyStr);
		Entity tweet = null;
		try {
			tweet = ds.get(key);
		} catch (EntityNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		request.setAttribute("message", tweet.getProperty("message"));
		request.setAttribute("author", tweet.getProperty("author"));
		request.setAttribute("date", tweet.getProperty("Date"));
		
		if (!tweet.getProperty("author").toString().equals(userName)) {
			int count = Long.valueOf((long)tweet.getProperty("view")).intValue() + 1;
			tweet.setProperty("view", count);
			ds.put(tweet);
		}
		request.setAttribute("view", tweet.getProperty("view"));
        RequestDispatcher jsp = request.getRequestDispatcher("/WEB-INF/info.jsp");
		jsp.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
